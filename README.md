<h1 align="center">Welcome to Project API Paris 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Front du site de collecte de la plateforme open data

### 🏠 [Homepage](https://gitlab.com/opendata-paris/app)

## Installation

Cloner le projet
```sh
git clone https://gitlab.com/opendata-paris/app.git
```
Installer les dépendances

```sh
npm install
```

## Usage

Pour démarrer l'application, lancer la commande suivante :
```sh
npm start
```

## Run tests

Pour lancer les tests (les tests automatiques seront lancés en cas de nouveau commit) 
```sh
npm run test
```

## Author

* 👤 **Baptiste Bivaud**
* 👤 **Arthur Bosshardt**
* 👤 **Quentin Ganacheau**
* 👤 **Johann Medjo**
* 👤 **Théo Viardin**

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_