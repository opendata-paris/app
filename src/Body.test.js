import React from "react";
import { render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Body from "./components/Body";

it("submit button disabled onClick", async () => {
    act(()=> {
      render(<Body />);
    })
  
    const button = document.querySelector("#dataform > button");
    
    expect(button).not.toBeDisabled();
    expect(button.textContent).toBe("Envoyer");
  
    act(()=> {
      button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    })
    
    expect(button).toBeDisabled();
    expect(button.textContent).not.toBe("Envoyer");
  });