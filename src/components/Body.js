import React from 'react';
import './Body.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure()

class Body extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            buttonActivity: false,
        };

        this.submit = this.submit.bind(this);
        this.fileInput = React.createRef();
    }

    render() {
        let button;

        if (this.state.buttonActivity) {
            button = (
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            );
        } else {
            button = 'Envoyer'
        }

        return (
            <div className="jumbotron container" style={{marginTop: 5}}>
                <h1 className="display-4">Insérer des données</h1>
                <p className="lead">Envoyez un fichier CSV pour ajouter des données dans la base OpenData !</p>
                <hr className="my-4"></hr>
                <form onSubmit={this.submit} id="dataform">
                    <div className="custom-file">
                        <input type="file" className="custom-file-input" id="validatedCustomFile" name="file" ref={this.fileInput} required />
                        <label className="custom-file-label" htmlFor="validatedCustomFile">Choisir un fichier...</label>
                    </div>
                    <button type="submit" className="btn btn-primary" disabled={this.state.buttonActivity}>{button}</button>
                </form>
            </div>
        );
    }

    submit(e) {
        
        const formData  = new FormData();

        formData.append('file', this.fileInput.current.files[0])
        
        this.setState({
            buttonActivity: true
        })

        fetch('http://localhost:5000/file', {
            method: 'POST',
            body: formData
        })
        .then(res => res.json())
        .then(() => {        
            this.setState({
                buttonActivity: false
            })

            toast.info('Synchronisé !')
        })
        .catch(err => console.log(err))

        e.preventDefault()
    }
}

export default Body;
