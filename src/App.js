import React from 'react';
import './App.css';

import Header from './components/Header.js';
import Body from './components/Body.js';

import 'bootstrap/dist/css/bootstrap.css';

function App() {
    return (
        <div className="App">
            <Header />
            <Body />
        </div>
    );
}

export default App;
