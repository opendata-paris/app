import React from "react";
import { generateImage, setDefaultOptions } from "jsdom-screenshot";
import { render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import App from "./App";

it("has less than 2% visual difference", async () => {
  setDefaultOptions({
    launch: { args: process.env.CI === "true" ? ["--no-sandbox"] : [] }
  });

  act(() => {
    render(<App />);
  });

  const screenshot = await generateImage(); 
  
  expect(screenshot).toMatchImageSnapshot({
    failureThreshold: 0.02,
    failureThresholdType: 'percent'
  });
});